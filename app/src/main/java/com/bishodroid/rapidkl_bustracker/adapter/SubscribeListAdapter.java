package com.bishodroid.rapidkl_bustracker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.bishodroid.rapidkl_bustracker.R;
import com.bishodroid.rapidkl_bustracker.bean.Bus;

import java.util.List;


public class SubscribeListAdapter extends BaseAdapter implements OnClickListener {
    private List<Bus> _buses;
    private Context _context;
    private TextView from;
    private LayoutInflater inflater;
    private boolean isSubscribed;
    private int limit;
    private Button route;
    private Button subscribe;
    private TextView to;

    public void onClick(View view) {
        if (SubscribeListAdapter.this.isSubscribed) {
            SubscribeListAdapter.this.unSubscribe();
            SubscribeListAdapter.this.updateButton();
            return;
        }
        SubscribeListAdapter.this.doSubscribe();
        SubscribeListAdapter.this.updateButton();
    }

    public SubscribeListAdapter(Context context, List<Bus> buses) {
        this.limit = 3;
        this._buses = buses;
        this._context = context;
    }

    public int getCount() {
        return this._buses.size();
    }

    public Object getItem(int i) {
        return this._buses.get(i);
    }

    public long getItemId(int i) {
        return (long) i;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (this.inflater == null) {
            this.inflater = (LayoutInflater) this._context.getSystemService("layout_inflater");
        }
        if (view == null) {
            view = this.inflater.inflate(R.layout.subscribe_item, null);
        }
        init(view);
        this.isSubscribed = false;
        String startinPoint = this._context.getResources().getString(R.string.txt_from) + this._buses.get(i).getStartingPoint();
        String destination = this._context.getResources().getString(R.string.txt_to) + this._buses.get(i).getDestination();
        this.route.setText(this._buses.get(i).getRouteName());
        this.from.setText(startinPoint);
        this.to.setText(destination);
        this.subscribe.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return view;
    }

    private void init(View view) {
        this.route = (Button) view.findViewById(R.id.route_name);
        this.from = (TextView) view.findViewById(R.id.txt_route_from);
        this.to = (TextView) view.findViewById(R.id.txt_route_to);
        this.subscribe = (Button) view.findViewById(R.id.btn_subscribe);
    }

    private void doSubscribe() {
    }

    private void unSubscribe() {
    }

    private void updateButton() {
        if (this.isSubscribed) {
            this.isSubscribed = false;
            this.subscribe.setText(this._context.getResources().getString(R.string.txt_unsubscribe));
            return;
        }
        this.isSubscribed = true;
        this.subscribe.setText(this._context.getResources().getString(R.string.txt_subscribe));
    }
}
