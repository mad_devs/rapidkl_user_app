package com.bishodroid.rapidkl_bustracker.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.bishodroid.rapidkl_bustracker.bean.User;
import com.google.gson.Gson;

public class Preferences {
    private static Preferences singleton;
    private Context _context;
    private Editor editor;
    private Gson gson;
    private SharedPreferences prefs;

    private Preferences(Context context) {
        this._context = context;
        Context context2 = this._context;
        String str = Constants.APP_SETTINGS;
        Context context3 = this._context;
        this.prefs = context2.getSharedPreferences(str, 0);
        this.gson = new Gson();
    }

    public static synchronized Preferences getInstance(Context context) {
        Preferences preferences;
        synchronized (Preferences.class) {
            if (singleton == null) {
                singleton = new Preferences(context);
            }
            preferences = singleton;
        }
        return preferences;
    }

    public User getUser() {
        return this.gson.fromJson(this.prefs.getString(Constants.USER_KEY, Constants.EMPTY_STRING), User.class);
    }

    public void putUser(User userJson) {
        this.editor.putString(Constants.USER_KEY, this.gson.toJson(userJson));
    }
}
