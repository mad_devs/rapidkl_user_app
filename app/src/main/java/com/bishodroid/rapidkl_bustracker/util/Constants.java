package com.bishodroid.rapidkl_bustracker.util;

public class Constants {
    public static final String APP_SETTINGS = "rapid_bus_tracker_settings";
    public static final String BUS_ROUTE = "bus_route";
    public static final String EMPTY_STRING = "";
    public static final String USER_KEY = "user_key";
    public static final String WELCOME = "Welcome back, ";
}
