package com.bishodroid.rapidkl_bustracker.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import com.bishodroid.rapidkl_bustracker.R;
import com.bishodroid.rapidkl_bustracker.fragment.Profile;
import com.bishodroid.rapidkl_bustracker.fragment.RapidKLMap;
import com.bishodroid.rapidkl_bustracker.fragment.Subscribe;

import java.util.ArrayList;
import java.util.List;

public class Main extends AppCompatActivity {
    private FloatingActionButton fab;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private ViewPager viewPager;

    /* renamed from: com.bishodroid.rapidklbus.activities.Main.1 */
    class C02021 implements OnClickListener {
        C02021() {
        }

        public void onClick(View view) {
            Snackbar.make(view, "Replace with your own action", 0).setAction("Action", null).show();
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList;
        private final List<String> mFragmentTitleList;

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
            this.mFragmentList = new ArrayList();
            this.mFragmentTitleList = new ArrayList();
        }

        public Fragment getItem(int position) {
            return this.mFragmentList.get(position);
        }

        public int getCount() {
            return this.mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            this.mFragmentList.add(fragment);
            this.mFragmentTitleList.add(title);
        }

        public CharSequence getPageTitle(int position) {
            return this.mFragmentTitleList.get(position);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGui();
    }

    private void initGui() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(this.toolbar);
        this.viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(this.viewPager);
        this.tabLayout = (TabLayout) findViewById(R.id.tabs);
        this.tabLayout.setupWithViewPager(this.viewPager);
        setupTabIcons();
        this.tabLayout.getTabAt(1).select();
        this.fab = (FloatingActionButton) findViewById(R.id.fab);
        this.fab.setOnClickListener(new C02021());
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new Subscribe(), "Subscribe");
        adapter.addFragment(new RapidKLMap(), "Routes Map");
        adapter.addFragment(new Profile(), "Profile");
        viewPager.setAdapter(adapter);
    }

    private void setupTabIcons() {
        View tabOne = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setBackgroundDrawable(0, R.drawable.ic_subscribe, 0, 0);
        this.tabLayout.getTabAt(0).setCustomView(tabOne);
        View tabTwo = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setBackgroundDrawable(0, R.drawable.ic_map, 0, 0);
        this.tabLayout.getTabAt(1).setCustomView(tabTwo);
        View tabThree = LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setBackgroundDrawable(0, R.drawable.ic_profile, 0, 0);
        this.tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
