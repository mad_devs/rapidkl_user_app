package com.bishodroid.rapidkl_bustracker.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.bishodroid.rapidkl_bustracker.R;

public class Register extends AppCompatActivity implements OnClickListener {
    private Button createAccount;
    private Button skip;
    private Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((R.layout.activity_register);
        initGUI();
    }

    private void initGUI() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar.setLogo(R.mipmap.ic_launcher);
        this.toolbar.setTitle(R.string.txt_create_account);
        setSupportActionBar(this.toolbar);
        this.createAccount.setOnClickListener(this);
        this.skip = (Button) findViewById(R.id.btn_skip);
        this.skip.setOnClickListener(this);
    }

    private void doSkip() {
        startActivity(new Intent(this, Main.class));
    }

    private void createAccount() {
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_skip /*2131492985*/:
                doSkip();
            case R.id.btn_create_account /*2131492990*/:
                doSkip();
            default:
        }
    }
}
