package com.bishodroid.rapidkl_bustracker.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.bishodroid.rapidkl_bustracker.R;
import com.bishodroid.rapidkl_bustracker.bean.User;
import com.bishodroid.rapidkl_bustracker.util.Constants;
import com.bishodroid.rapidkl_bustracker.util.Preferences;

public class Splash extends AppCompatActivity {
    private TextView appDevs;
    private TextView appVersion;
    private PackageInfo info;
    private Preferences prefs;
    private ImageView splashImg;
    private User user;
    private boolean userExists;
    private TextView welcome;

    public void run() {
        Intent intent;
        try {
            Thread.sleep(3000);
            if (Splash.this.userExists) {
                intent = new Intent(Splash.this, Main.class);
                intent.putExtra(Constants.USER_KEY, Splash.this.user);
                Splash.this.startActivity(intent);
                Splash.this.finish();
                return;
            }
            Splash.this.startActivity(new Intent(Splash.this, Register.class));
            Splash.this.finish();
        } catch (InterruptedException e) {
            e.printStackTrace();
            if (Splash.this.userExists) {
                intent = new Intent(Splash.this, Main.class);
                intent.putExtra(Constants.USER_KEY, Splash.this.user);
                Splash.this.startActivity(intent);
                Splash.this.finish();
                return;
            }
            Splash.this.startActivity(new Intent(Splash.this, Register.class));
            Splash.this.finish();
        } catch (Throwable th) {
            if (Splash.this.userExists) {
                intent = new Intent(Splash.this, Main.class);
                intent.putExtra(Constants.USER_KEY, Splash.this.user);
                Splash.this.startActivity(intent);
                Splash.this.finish();
            } else {
                Splash.this.startActivity(new Intent(Splash.this, Register.class));
                Splash.this.finish();
            }
        }
    }

    public Splash() {
        this.userExists = false;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initGui();
        this.prefs = Preferences.getInstance(getApplicationContext());
        this.user = this.prefs.getUser();
        if (this.user != null) {
            this.userExists = true;
        }
        setupSplash();
    }

    private void initGui() {
        this.welcome = (TextView) findViewById(R.id.lbl_welcome);
        this.appVersion = (TextView) findViewById(R.id.lbl_app_version);
        this.appDevs = (TextView) findViewById(R.id.lbl_developers);
        this.splashImg = (ImageView) findViewById(R.id.img_splash);
        this.appVersion.setText(getResources().getString(R.string.version) + getAppVersion());
        if (this.userExists) {
            this.welcome.setText(getResources().getString(R.string.welcome_msg) + this.user.getName());
        }
    }

    private String getAppVersion() {
        String version = Constants.EMPTY_STRING;
        try {
            this.info = getPackageManager().getPackageInfo(getPackageName(), 0);
            return this.info.versionName;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return version;
        }
    }

    private void setupSplash() {
        new C02031().start();
    }
}
