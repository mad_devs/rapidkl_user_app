package com.bishodroid.rapidkl_bustracker.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.bishodroid.rapidkl_bustracker.R;
import com.bishodroid.rapidkl_bustracker.activity.BusRouteInfo;
import com.bishodroid.rapidkl_bustracker.adapter.SubscribeListAdapter;
import com.bishodroid.rapidkl_bustracker.bean.Bus;
import com.bishodroid.rapidkl_bustracker.util.Constants;

import java.util.ArrayList;
import java.util.List;

public class Subscribe extends Fragment implements OnRefreshListener {
    private SubscribeListAdapter adapter;
    private List<Bus> buses;
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;

    /* renamed from: com.bishodroid.rapidklbus.fragments.Subscribe.1 */
    class C02051 implements OnItemClickListener {
        C02051() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Bus bus = Subscribe.this.buses.get(position);
            Intent intent = new Intent(Subscribe.this.getActivity(), BusRouteInfo.class);
            intent.putExtra(Constants.BUS_ROUTE, bus);
            Subscribe.this.startActivity(intent);
        }
    }

    /* renamed from: com.bishodroid.rapidklbus.fragments.Subscribe.2 */
    class C02062 implements Runnable {
        C02062() {
        }

        public void run() {
            Subscribe.this.swipeRefreshLayout.setRefreshing(true);
            Subscribe.this.getArticles();
            Subscribe.this.swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subscribe, container, false);
        this.listView = (ListView) v.findViewById(R.id.listView);
        this.swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_refresh_layout);
        this.buses = createMockBuses(10);
        this.adapter = new SubscribeListAdapter(getActivity(), this.buses);
        this.listView.setAdapter(this.adapter);
        this.listView.setOnItemClickListener(new C02051());
        this.swipeRefreshLayout.setOnRefreshListener(this);
        this.swipeRefreshLayout.post(new C02062());
        return v;
    }

    public void onRefresh() {
        getArticles();
    }

    private List<Bus> createMockBuses(int count) {
        this.swipeRefreshLayout.setColorSchemeColors(R.color.colorPrimary, R.color.colorDarkGray);
        List<Bus> buses = new ArrayList();
        for (int i = 0; i < count; i++) {
            Bus bus = new Bus();
            bus.setRouteName("301");
            bus.setStartingPoint("Ampang");
            bus.setDestination("Pasar Sini");
            buses.add(bus);
        }
        return buses;
    }

    private void getArticles() {
        this.swipeRefreshLayout.setRefreshing(true);
        this.buses = createMockBuses(10);
        this.swipeRefreshLayout.setRefreshing(false);
    }
}
