package com.bishodroid.rapidkl_bustracker.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bishodroid.rapidklbus.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition.Builder;
import com.google.android.gms.maps.model.LatLng;

public class RapidKLMap extends Fragment {
    private FragmentManager fragmentManager;
    private GoogleMap map;
    private SupportMapFragment mapFragment;
    private FragmentTransaction transaction;

    /* renamed from: com.bishodroid.rapidklbus.fragments.RapidKLMap.1 */
    class C03641 implements OnMapReadyCallback {
        C03641() {
        }

        public void onMapReady(GoogleMap googleMap) {
            if (googleMap != null) {
                googleMap.getUiSettings().setAllGesturesEnabled(true);
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                googleMap.getUiSettings().setMapToolbarEnabled(true);
                googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(new Builder().target(new LatLng(311.321d, 321.221d)).zoom(BitmapDescriptorFactory.HUE_ORANGE).build()));
            }
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_rapid_klmap, container, false);
        this.mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.rapid_kl_map);
        if (this.mapFragment == null) {
            this.fragmentManager = getChildFragmentManager();
            this.transaction = this.fragmentManager.beginTransaction();
            this.transaction.replace(R.id.rapid_kl_map, this.mapFragment);
            this.mapFragment.getMapAsync(new C03641());
        }
        return v;
    }
}
