package com.bishodroid.rapidkl_bustracker.bean;

import java.io.Serializable;

public class LocationCords implements Serializable {
    private double latitude;
    private String location;
    private double longtitude;

    public double getLatitude() {
        return this.latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return this.longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String toString() {
        return "LocationCords{latitude=" + this.latitude + ", longtitude=" + this.longtitude + ", location='" + this.location + '\'' + '}';
    }
}
