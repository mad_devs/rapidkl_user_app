package com.bishodroid.rapidkl_bustracker.bean;

import java.io.Serializable;
import java.util.List;

public class Bus implements Serializable {
    private String destination;
    private String routeName;
    private String startingPoint;
    private List<User> subscribers;

    public String getRouteName() {
        return this.routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getStartingPoint() {
        return this.startingPoint;
    }

    public void setStartingPoint(String startingPoint) {
        this.startingPoint = startingPoint;
    }

    public String getDestination() {
        return this.destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public List<User> getSubscribers() {
        return this.subscribers;
    }

    public void setSubscribers(List<User> subscribers) {
        this.subscribers = subscribers;
    }

    public String toString() {
        return "Bus{routeName='" + this.routeName + '\'' + ", startingPoint='" + this.startingPoint + '\'' + ", destination='" + this.destination + '\'' + ", subscribers=" + this.subscribers + '}';
    }
}
