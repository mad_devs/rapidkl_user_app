package com.bishodroid.rapidkl_bustracker.bean;

import java.io.Serializable;

public class User implements Serializable {
    private LocationCords currentLocation;
    private String email;
    private String imageUrl;
    private String name;
    private int subscribes;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public LocationCords getCurrentLocation() {
        return this.currentLocation;
    }

    public void setCurrentLocation(LocationCords currentLocation) {
        this.currentLocation = currentLocation;
    }

    public int getSubscribes() {
        return this.subscribes;
    }

    public void setSubscribes(int subscribes) {
        this.subscribes = subscribes;
    }

    public String toString() {
        return "User{name='" + this.name + '\'' + ", email='" + this.email + '\'' + ", imageUrl='" + this.imageUrl + '\'' + ", currentLocation=" + this.currentLocation + ", subscribes=" + this.subscribes + '}';
    }
}
